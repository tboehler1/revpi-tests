ifndef DUT
	$(error "Must define which DUT is tested")
endif

CONFIG ?= configs/$(DUT).conf
RSC ?= files/$(DUT).rsc

check: Kyuafile $(CONFIG) $(RSC)
	kyua --config $(CONFIG) --variable parallelism=$$(nproc) test \
		|| (kyua report --verbose && exit 1)
