{
  "App": {
    "name": "PiCtory",
    "version": "2.11.0",
    "saveTS": "20241209150936",
    "language": "en",
    "layout": {
      "north": {
        "size": 70,
        "initClosed": false,
        "initHidden": false
      },
      "south": {
        "size": 476,
        "initClosed": false,
        "initHidden": false,
        "children": {
          "layout1": {
            "east": {
              "size": 500,
              "initClosed": false,
              "initHidden": false
            }
          }
        }
      },
      "east": {
        "size": 70,
        "initClosed": true,
        "initHidden": false,
        "children": {}
      },
      "west": {
        "size": 258,
        "initClosed": false,
        "initHidden": false,
        "children": {
          "layout1": {}
        }
      }
    }
  },
  "Summary": {
    "inpTotal": 201,
    "outTotal": 75
  },
  "Devices": [
    {
      "GUID": "decd632f-ab37-066b-ae17-7ef0b5c1b2b9",
      "id": "device_RevPiDO_20160818_1_0_001",
      "type": "LEFT_RIGHT",
      "productType": "98",
      "position": "29",
      "name": "RevPi DO",
      "bmk": "RevPi DO",
      "inpVariant": 0,
      "outVariant": 0,
      "comment": "",
      "offset": 0,
      "inp": {
        "0": [
          "Output_Status_i04",
          "0",
          "16",
          "2",
          false,
          "0000",
          "",
          ""
        ],
        "1": [
          "Status_i04",
          "0",
          "16",
          "4",
          false,
          "0001",
          "",
          ""
        ],
        "2": [
          "Input",
          "null",
          "16",
          "0",
          false,
          "0038",
          "",
          ""
        ],
        "3": [
          "Counter_1_i04",
          "null",
          "32",
          "6",
          false,
          "0039",
          "",
          ""
        ],
        "4": [
          "Counter_2_i04",
          "null",
          "32",
          "10",
          false,
          "0040",
          "",
          ""
        ],
        "5": [
          "Counter_3_i04",
          "null",
          "32",
          "14",
          false,
          "0041",
          "",
          ""
        ],
        "6": [
          "Counter_4_i04",
          "null",
          "32",
          "18",
          false,
          "0042",
          "",
          ""
        ],
        "7": [
          "Counter_5_i04",
          "null",
          "32",
          "22",
          false,
          "0043",
          "",
          ""
        ],
        "8": [
          "Counter_6_i04",
          "null",
          "32",
          "26",
          false,
          "0044",
          "",
          ""
        ],
        "9": [
          "Counter_7_i04",
          "null",
          "32",
          "30",
          false,
          "0045",
          "",
          ""
        ],
        "10": [
          "Counter_8_i04",
          "null",
          "32",
          "34",
          false,
          "0046",
          "",
          ""
        ],
        "11": [
          "Counter_9_i04",
          "null",
          "32",
          "38",
          false,
          "0047",
          "",
          ""
        ],
        "12": [
          "Counter_10_i04",
          "null",
          "32",
          "42",
          false,
          "0048",
          "",
          ""
        ],
        "13": [
          "Counter_11_i04",
          "null",
          "32",
          "46",
          false,
          "0049",
          "",
          ""
        ],
        "14": [
          "Counter_12_i04",
          "null",
          "32",
          "50",
          false,
          "0050",
          "",
          ""
        ],
        "15": [
          "Counter_13_i04",
          "null",
          "32",
          "54",
          false,
          "0051",
          "",
          ""
        ],
        "16": [
          "Counter_14_i04",
          "null",
          "32",
          "58",
          false,
          "0052",
          "",
          ""
        ],
        "17": [
          "Counter_15_i04",
          "null",
          "32",
          "62",
          false,
          "0053",
          "",
          ""
        ],
        "18": [
          "Counter_16_i04",
          "null",
          "32",
          "66",
          false,
          "0054",
          "",
          ""
        ]
      },
      "out": {
        "0": [
          "DO_L3_O1",
          "0",
          "1",
          "70",
          true,
          "0002",
          "",
          "0"
        ],
        "1": [
          "DO_L3_O2",
          "0",
          "1",
          "70",
          true,
          "0003",
          "",
          "1"
        ],
        "2": [
          "DO_L3_O3",
          "0",
          "1",
          "70",
          true,
          "0004",
          "",
          "2"
        ],
        "3": [
          "DO_L3_O4",
          "0",
          "1",
          "70",
          true,
          "0005",
          "",
          "3"
        ],
        "4": [
          "DO_L3_O5",
          "0",
          "1",
          "70",
          true,
          "0006",
          "",
          "4"
        ],
        "5": [
          "DO_L3_O6",
          "0",
          "1",
          "70",
          true,
          "0007",
          "",
          "5"
        ],
        "6": [
          "DO_L3_O7",
          "0",
          "1",
          "70",
          true,
          "0008",
          "",
          "6"
        ],
        "7": [
          "DO_L3_O8",
          "0",
          "1",
          "70",
          true,
          "0009",
          "",
          "7"
        ],
        "8": [
          "DO_L3_O9",
          "0",
          "1",
          "70",
          true,
          "0010",
          "",
          "8"
        ],
        "9": [
          "DO_L3_O10",
          "0",
          "1",
          "70",
          true,
          "0011",
          "",
          "9"
        ],
        "10": [
          "DO_L3_O11",
          "0",
          "1",
          "70",
          true,
          "0012",
          "",
          "10"
        ],
        "11": [
          "DO_L3_O12",
          "0",
          "1",
          "70",
          true,
          "0013",
          "",
          "11"
        ],
        "12": [
          "DO_L3_O13",
          "0",
          "1",
          "70",
          true,
          "0014",
          "",
          "12"
        ],
        "13": [
          "DO_L3_O14",
          "0",
          "1",
          "70",
          true,
          "0015",
          "",
          "13"
        ],
        "14": [
          "DO_L3_O15",
          "0",
          "1",
          "70",
          true,
          "0016",
          "",
          "14"
        ],
        "15": [
          "DO_L3_O16",
          "0",
          "1",
          "70",
          true,
          "0017",
          "",
          "15"
        ],
        "16": [
          "PWM_1_i04",
          "0",
          "8",
          "72",
          false,
          "0018",
          "",
          ""
        ],
        "17": [
          "PWM_2_i04",
          "0",
          "8",
          "73",
          false,
          "0019",
          "",
          ""
        ],
        "18": [
          "PWM_3_i04",
          "0",
          "8",
          "74",
          false,
          "0020",
          "",
          ""
        ],
        "19": [
          "PWM_4_i04",
          "0",
          "8",
          "75",
          false,
          "0021",
          "",
          ""
        ],
        "20": [
          "PWM_5_i04",
          "0",
          "8",
          "76",
          false,
          "0022",
          "",
          ""
        ],
        "21": [
          "PWM_6_i04",
          "0",
          "8",
          "77",
          false,
          "0023",
          "",
          ""
        ],
        "22": [
          "PWM_7_i04",
          "0",
          "8",
          "78",
          false,
          "0024",
          "",
          ""
        ],
        "23": [
          "PWM_8_i04",
          "0",
          "8",
          "79",
          false,
          "0025",
          "",
          ""
        ],
        "24": [
          "PWM_9_i04",
          "0",
          "8",
          "80",
          false,
          "0026",
          "",
          ""
        ],
        "25": [
          "PWM_10_i04",
          "0",
          "8",
          "81",
          false,
          "0027",
          "",
          ""
        ],
        "26": [
          "PWM_11_i04",
          "0",
          "8",
          "82",
          false,
          "0028",
          "",
          ""
        ],
        "27": [
          "PWM_12_i04",
          "0",
          "8",
          "83",
          false,
          "0029",
          "",
          ""
        ],
        "28": [
          "PWM_13_i04",
          "0",
          "8",
          "84",
          false,
          "0030",
          "",
          ""
        ],
        "29": [
          "PWM_14_i04",
          "0",
          "8",
          "85",
          false,
          "0031",
          "",
          ""
        ],
        "30": [
          "PWM_15_i04",
          "0",
          "8",
          "86",
          false,
          "0032",
          "",
          ""
        ],
        "31": [
          "PWM_16_i04",
          "0",
          "8",
          "87",
          false,
          "0033",
          "",
          ""
        ]
      },
      "mem": {
        "0": [
          "OutputPushPull_i04",
          "0",
          "16",
          "106",
          false,
          "0034",
          "",
          ""
        ],
        "1": [
          "OutputOpenLoadDetect_i04",
          "0",
          "16",
          "108",
          false,
          "0035",
          "",
          ""
        ],
        "2": [
          "OutputPWMActive_i04",
          "0",
          "16",
          "110",
          false,
          "0036",
          "",
          ""
        ],
        "3": [
          "OutputPWMFrequency_i04",
          "1",
          "8",
          "112",
          false,
          "0037",
          "",
          ""
        ],
        "4": [
          "InputMode_1_i04",
          "0",
          "8",
          "88",
          false,
          "0055",
          "",
          ""
        ],
        "5": [
          "InputMode_2_i04",
          "0",
          "8",
          "89",
          false,
          "0056",
          "",
          ""
        ],
        "6": [
          "InputMode_3_i04",
          "0",
          "8",
          "90",
          false,
          "0057",
          "",
          ""
        ],
        "7": [
          "InputMode_4_i04",
          "0",
          "8",
          "91",
          false,
          "0058",
          "",
          ""
        ],
        "8": [
          "InputMode_5_i04",
          "0",
          "8",
          "92",
          false,
          "0059",
          "",
          ""
        ],
        "9": [
          "InputMode_6_i04",
          "0",
          "8",
          "93",
          false,
          "0060",
          "",
          ""
        ],
        "10": [
          "InputMode_7_i04",
          "0",
          "8",
          "94",
          false,
          "0061",
          "",
          ""
        ],
        "11": [
          "InputMode_8_i04",
          "0",
          "8",
          "95",
          false,
          "0062",
          "",
          ""
        ],
        "12": [
          "InputMode_9_i04",
          "0",
          "8",
          "96",
          false,
          "0063",
          "",
          ""
        ],
        "13": [
          "InputMode_10_i04",
          "0",
          "8",
          "97",
          false,
          "0064",
          "",
          ""
        ],
        "14": [
          "InputMode_11_i04",
          "0",
          "8",
          "98",
          false,
          "0065",
          "",
          ""
        ],
        "15": [
          "InputMode_12_i04",
          "0",
          "8",
          "99",
          false,
          "0066",
          "",
          ""
        ],
        "16": [
          "InputMode_13_i04",
          "0",
          "8",
          "100",
          false,
          "0067",
          "",
          ""
        ],
        "17": [
          "InputMode_14_i04",
          "0",
          "8",
          "101",
          false,
          "0068",
          "",
          ""
        ],
        "18": [
          "InputMode_15_i04",
          "0",
          "8",
          "102",
          false,
          "0069",
          "",
          ""
        ],
        "19": [
          "InputMode_16_i04",
          "0",
          "8",
          "103",
          false,
          "0070",
          "",
          ""
        ],
        "20": [
          "InputDebounce_i04",
          "0",
          "16",
          "104",
          false,
          "0071",
          "",
          ""
        ]
      },
      "extend": {}
    },
    {
      "GUID": "dcc782d6-8bfc-43d7-0f3e-c8fa65654a1f",
      "id": "device_RevPiRO_20231018_1_0_001",
      "type": "LEFT_RIGHT",
      "productType": "137",
      "position": "30",
      "name": "RevPi RO",
      "bmk": "RevPi RO",
      "inpVariant": 0,
      "outVariant": 0,
      "comment": "",
      "offset": 113,
      "inp": {
        "0": [
          "Status_i03",
          "0",
          "8",
          "0",
          false,
          "0000",
          "",
          ""
        ]
      },
      "out": {
        "0": [
          "RO_L2_O1",
          "0",
          "1",
          "1",
          true,
          "0001",
          "",
          "0"
        ],
        "1": [
          "RO_L2_O2",
          "0",
          "1",
          "1",
          true,
          "0002",
          "",
          "1"
        ],
        "2": [
          "RO_L2_O3",
          "0",
          "1",
          "1",
          true,
          "0003",
          "",
          "2"
        ],
        "3": [
          "RO_L2_O4",
          "0",
          "1",
          "1",
          true,
          "0004",
          "",
          "3"
        ],
        "4": [
          "RelayOutputPadding_5",
          "null",
          "1",
          "1",
          true,
          "0005",
          "",
          "4"
        ],
        "5": [
          "RelayOutputPadding_6",
          "null",
          "1",
          "1",
          true,
          "0006",
          "",
          "5"
        ],
        "6": [
          "RelayOutputPadding_7",
          "null",
          "1",
          "1",
          true,
          "0007",
          "",
          "6"
        ],
        "7": [
          "RelayOutputPadding_8",
          "null",
          "1",
          "1",
          true,
          "0008",
          "",
          "7"
        ]
      },
      "mem": {
        "0": [
          "RelayCycleWarningThreshold_1",
          "0",
          "32",
          "2",
          false,
          "0009",
          "",
          ""
        ],
        "1": [
          "RelayCycleWarningThreshold_2",
          "0",
          "32",
          "6",
          false,
          "0010",
          "",
          ""
        ],
        "2": [
          "RelayCycleWarningThreshold_3",
          "0",
          "32",
          "10",
          false,
          "0011",
          "",
          ""
        ],
        "3": [
          "RelayCycleWarningThreshold_4",
          "0",
          "32",
          "14",
          false,
          "0012",
          "",
          ""
        ]
      },
      "extend": {}
    },
    {
      "GUID": "47141ce3-3a86-00b7-250d-05aed71fe58c",
      "id": "device_RevPiDI_20160818_1_0_001",
      "type": "LEFT_RIGHT",
      "productType": "97",
      "position": "31",
      "name": "RevPi DI",
      "bmk": "RevPi DI",
      "inpVariant": 0,
      "outVariant": 0,
      "comment": "",
      "offset": 131,
      "inp": {
        "0": [
          "DI_L1_I1",
          "0",
          "1",
          "0",
          true,
          "0000",
          "",
          "0"
        ],
        "1": [
          "DI_L1_I2",
          "0",
          "1",
          "0",
          true,
          "0001",
          "",
          "1"
        ],
        "2": [
          "DI_L1_I3",
          "0",
          "1",
          "0",
          true,
          "0002",
          "",
          "2"
        ],
        "3": [
          "DI_L1_I4",
          "0",
          "1",
          "0",
          true,
          "0003",
          "",
          "3"
        ],
        "4": [
          "DI_L1_I5",
          "0",
          "1",
          "0",
          true,
          "0004",
          "",
          "4"
        ],
        "5": [
          "DI_L1_I6",
          "0",
          "1",
          "0",
          true,
          "0005",
          "",
          "5"
        ],
        "6": [
          "DI_L1_I7",
          "0",
          "1",
          "0",
          true,
          "0006",
          "",
          "6"
        ],
        "7": [
          "DI_L1_I8",
          "0",
          "1",
          "0",
          true,
          "0007",
          "",
          "7"
        ],
        "8": [
          "DI_L1_I9",
          "0",
          "1",
          "0",
          true,
          "0008",
          "",
          "8"
        ],
        "9": [
          "DI_L1_I10",
          "0",
          "1",
          "0",
          true,
          "0009",
          "",
          "9"
        ],
        "10": [
          "DI_L1_I11",
          "0",
          "1",
          "0",
          true,
          "0010",
          "",
          "10"
        ],
        "11": [
          "DI_L1_I12",
          "0",
          "1",
          "0",
          true,
          "0011",
          "",
          "11"
        ],
        "12": [
          "DI_L1_I13",
          "0",
          "1",
          "0",
          true,
          "0012",
          "",
          "12"
        ],
        "13": [
          "DI_L1_I14",
          "0",
          "1",
          "0",
          true,
          "0013",
          "",
          "13"
        ],
        "14": [
          "DI_L1_I15",
          "0",
          "1",
          "0",
          true,
          "0014",
          "",
          "14"
        ],
        "15": [
          "DI_L1_I16",
          "0",
          "1",
          "0",
          true,
          "0015",
          "",
          "15"
        ],
        "16": [
          "Status",
          "0",
          "16",
          "4",
          false,
          "0016",
          "",
          ""
        ],
        "17": [
          "Counter_1",
          "0",
          "32",
          "6",
          false,
          "0017",
          "",
          ""
        ],
        "18": [
          "Counter_2",
          "0",
          "32",
          "10",
          false,
          "0018",
          "",
          ""
        ],
        "19": [
          "Counter_3",
          "0",
          "32",
          "14",
          false,
          "0019",
          "",
          ""
        ],
        "20": [
          "Counter_4",
          "0",
          "32",
          "18",
          false,
          "0020",
          "",
          ""
        ],
        "21": [
          "Counter_5",
          "0",
          "32",
          "22",
          false,
          "0021",
          "",
          ""
        ],
        "22": [
          "Counter_6",
          "0",
          "32",
          "26",
          false,
          "0022",
          "",
          ""
        ],
        "23": [
          "Counter_7",
          "0",
          "32",
          "30",
          false,
          "0023",
          "",
          ""
        ],
        "24": [
          "Counter_8",
          "0",
          "32",
          "34",
          false,
          "0024",
          "",
          ""
        ],
        "25": [
          "Counter_9",
          "0",
          "32",
          "38",
          false,
          "0025",
          "",
          ""
        ],
        "26": [
          "Counter_10",
          "0",
          "32",
          "42",
          false,
          "0026",
          "",
          ""
        ],
        "27": [
          "Counter_11",
          "0",
          "32",
          "46",
          false,
          "0027",
          "",
          ""
        ],
        "28": [
          "Counter_12",
          "0",
          "32",
          "50",
          false,
          "0028",
          "",
          ""
        ],
        "29": [
          "Counter_13",
          "0",
          "32",
          "54",
          false,
          "0029",
          "",
          ""
        ],
        "30": [
          "Counter_14",
          "0",
          "32",
          "58",
          false,
          "0030",
          "",
          ""
        ],
        "31": [
          "Counter_15",
          "0",
          "32",
          "62",
          false,
          "0031",
          "",
          ""
        ],
        "32": [
          "Counter_16",
          "0",
          "32",
          "66",
          false,
          "0032",
          "",
          ""
        ],
        "33": [
          "Output_Status",
          "null",
          "16",
          "2",
          false,
          "0050",
          "",
          ""
        ]
      },
      "out": {
        "0": [
          "Output",
          "null",
          "16",
          "70",
          false,
          "0051",
          "",
          ""
        ],
        "1": [
          "PWM_1",
          "null",
          "8",
          "72",
          false,
          "0052",
          "",
          ""
        ],
        "2": [
          "PWM_2",
          "null",
          "8",
          "73",
          false,
          "0053",
          "",
          ""
        ],
        "3": [
          "PWM_3",
          "null",
          "8",
          "74",
          false,
          "0054",
          "",
          ""
        ],
        "4": [
          "PWM_4",
          "null",
          "8",
          "75",
          false,
          "0055",
          "",
          ""
        ],
        "5": [
          "PWM_5",
          "null",
          "8",
          "76",
          false,
          "0056",
          "",
          ""
        ],
        "6": [
          "PWM_6",
          "null",
          "8",
          "77",
          false,
          "0057",
          "",
          ""
        ],
        "7": [
          "PWM_7",
          "null",
          "8",
          "78",
          false,
          "0058",
          "",
          ""
        ],
        "8": [
          "PWM_8",
          "null",
          "8",
          "79",
          false,
          "0059",
          "",
          ""
        ],
        "9": [
          "PWM_9",
          "null",
          "8",
          "80",
          false,
          "0060",
          "",
          ""
        ],
        "10": [
          "PWM_10",
          "null",
          "8",
          "81",
          false,
          "0061",
          "",
          ""
        ],
        "11": [
          "PWM_11",
          "null",
          "8",
          "82",
          false,
          "0062",
          "",
          ""
        ],
        "12": [
          "PWM_12",
          "null",
          "8",
          "83",
          false,
          "0063",
          "",
          ""
        ],
        "13": [
          "PWM_13",
          "null",
          "8",
          "84",
          false,
          "0064",
          "",
          ""
        ],
        "14": [
          "PWM_14",
          "null",
          "8",
          "85",
          false,
          "0065",
          "",
          ""
        ],
        "15": [
          "PWM_15",
          "null",
          "8",
          "86",
          false,
          "0066",
          "",
          ""
        ],
        "16": [
          "PWM_16",
          "null",
          "8",
          "87",
          false,
          "0067",
          "",
          ""
        ]
      },
      "mem": {
        "0": [
          "InputMode_1",
          "0",
          "8",
          "88",
          false,
          "0033",
          "",
          ""
        ],
        "1": [
          "InputMode_2",
          "0",
          "8",
          "89",
          false,
          "0034",
          "",
          ""
        ],
        "2": [
          "InputMode_3",
          "0",
          "8",
          "90",
          false,
          "0035",
          "",
          ""
        ],
        "3": [
          "InputMode_4",
          "0",
          "8",
          "91",
          false,
          "0036",
          "",
          ""
        ],
        "4": [
          "InputMode_5",
          "0",
          "8",
          "92",
          false,
          "0037",
          "",
          ""
        ],
        "5": [
          "InputMode_6",
          "0",
          "8",
          "93",
          false,
          "0038",
          "",
          ""
        ],
        "6": [
          "InputMode_7",
          "0",
          "8",
          "94",
          false,
          "0039",
          "",
          ""
        ],
        "7": [
          "InputMode_8",
          "0",
          "8",
          "95",
          false,
          "0040",
          "",
          ""
        ],
        "8": [
          "InputMode_9",
          "0",
          "8",
          "96",
          false,
          "0041",
          "",
          ""
        ],
        "9": [
          "InputMode_10",
          "0",
          "8",
          "97",
          false,
          "0042",
          "",
          ""
        ],
        "10": [
          "InputMode_11",
          "0",
          "8",
          "98",
          false,
          "0043",
          "",
          ""
        ],
        "11": [
          "InputMode_12",
          "0",
          "8",
          "99",
          false,
          "0044",
          "",
          ""
        ],
        "12": [
          "InputMode_13",
          "0",
          "8",
          "100",
          false,
          "0045",
          "",
          ""
        ],
        "13": [
          "InputMode_14",
          "0",
          "8",
          "101",
          false,
          "0046",
          "",
          ""
        ],
        "14": [
          "InputMode_15",
          "0",
          "8",
          "102",
          false,
          "0047",
          "",
          ""
        ],
        "15": [
          "InputMode_16",
          "0",
          "8",
          "103",
          false,
          "0048",
          "",
          ""
        ],
        "16": [
          "InputDebounce",
          "0",
          "16",
          "104",
          false,
          "0049",
          "",
          ""
        ],
        "17": [
          "OutputPushPull",
          "null",
          "16",
          "106",
          false,
          "0068",
          "",
          ""
        ],
        "18": [
          "OutputOpenLoadDetect",
          "null",
          "16",
          "108",
          false,
          "0069",
          "",
          ""
        ],
        "19": [
          "OutputPWMActive",
          "null",
          "16",
          "110",
          false,
          "0070",
          "",
          ""
        ],
        "20": [
          "OutputPWMFrequency",
          "1",
          "8",
          "112",
          false,
          "0071",
          "",
          ""
        ]
      },
      "extend": {}
    },
    {
      "GUID": "40cae6f1-f7e2-211a-30ef-7c5668755733",
      "id": "device_RevPiConnect5_20240315_1_0_001",
      "type": "BASE",
      "productType": "138",
      "position": "0",
      "name": "RevPi Connect 5",
      "bmk": "RevPi Connect 5",
      "inpVariant": 0,
      "outVariant": 0,
      "comment": "This is a RevPi Connect 5 Device",
      "offset": 244,
      "inp": {
        "0": [
          "RevPiStatus",
          "0",
          "8",
          "0",
          true,
          "0000",
          "",
          ""
        ],
        "1": [
          "RevPiIOCycle",
          "0",
          "8",
          "1",
          true,
          "0001",
          "",
          ""
        ],
        "2": [
          "RS485ErrorCnt",
          "0",
          "16",
          "2",
          false,
          "0002",
          "",
          ""
        ],
        "3": [
          "Core_Temperature",
          "0",
          "8",
          "4",
          false,
          "0003",
          "",
          ""
        ],
        "4": [
          "Core_Frequency",
          "0",
          "8",
          "5",
          false,
          "0004",
          "",
          ""
        ]
      },
      "out": {
        "0": [
          "RevPiReservedByte",
          "",
          "8",
          "6",
          false,
          "0005",
          "",
          ""
        ],
        "1": [
          "RS485ErrorLimit1",
          "10",
          "16",
          "7",
          false,
          "0006",
          "",
          ""
        ],
        "2": [
          "RS485ErrorLimit2",
          "1000",
          "16",
          "9",
          false,
          "0007",
          "",
          ""
        ],
        "3": [
          "RevPiLED",
          "0",
          "16",
          "11",
          true,
          "0008",
          "",
          ""
        ]
      },
      "mem": {},
      "extend": {}
    },
    {
      "GUID": "b9c0e3c6-9188-ae75-d901-96b689e47753",
      "id": "device_RevPiAIO_20170301_1_0_001",
      "type": "LEFT_RIGHT",
      "productType": "103",
      "position": "32",
      "name": "RevPi AIO",
      "bmk": "RevPi AIO",
      "inpVariant": 0,
      "outVariant": 0,
      "comment": "",
      "offset": 257,
      "inp": {
        "0": [
          "AIO_R1_I1",
          "0",
          "16",
          "0",
          false,
          "0000",
          "",
          ""
        ],
        "1": [
          "AIO_R1_I2",
          "0",
          "16",
          "2",
          false,
          "0001",
          "",
          ""
        ],
        "2": [
          "AIO_R1_I3",
          "0",
          "16",
          "4",
          false,
          "0002",
          "",
          ""
        ],
        "3": [
          "AIO_R1_I4",
          "0",
          "16",
          "6",
          false,
          "0003",
          "",
          ""
        ],
        "4": [
          "InputStatus_1",
          "0",
          "8",
          "8",
          false,
          "0004",
          "",
          ""
        ],
        "5": [
          "InputStatus_2",
          "0",
          "8",
          "9",
          false,
          "0005",
          "",
          ""
        ],
        "6": [
          "InputStatus_3",
          "0",
          "8",
          "10",
          false,
          "0006",
          "",
          ""
        ],
        "7": [
          "InputStatus_4",
          "0",
          "8",
          "11",
          false,
          "0007",
          "",
          ""
        ],
        "8": [
          "RTDValue_1",
          "0",
          "16",
          "12",
          false,
          "0008",
          "",
          ""
        ],
        "9": [
          "RTDValue_2",
          "0",
          "16",
          "14",
          false,
          "0009",
          "",
          ""
        ],
        "10": [
          "RTDStatus_1",
          "0",
          "8",
          "16",
          false,
          "0010",
          "",
          ""
        ],
        "11": [
          "RTDStatus_2",
          "0",
          "8",
          "17",
          false,
          "0011",
          "",
          ""
        ],
        "12": [
          "OutputStatus_1",
          "0",
          "8",
          "18",
          false,
          "0012",
          "",
          ""
        ],
        "13": [
          "OutputStatus_2",
          "0",
          "8",
          "19",
          false,
          "0013",
          "",
          ""
        ]
      },
      "out": {
        "0": [
          "AIO_R1_O1",
          "0",
          "16",
          "20",
          false,
          "0014",
          "",
          ""
        ],
        "1": [
          "AIO_R1_O2",
          "0",
          "16",
          "22",
          false,
          "0015",
          "",
          ""
        ]
      },
      "mem": {
        "0": [
          "Input1Range",
          "1",
          "8",
          "24",
          false,
          "0016",
          "You must use wire bridges for current measurement!",
          ""
        ],
        "1": [
          "Input1Multiplier",
          "1",
          "16",
          "25",
          false,
          "0017",
          "",
          ""
        ],
        "2": [
          "Input1Divisor",
          "1",
          "16",
          "27",
          false,
          "0018",
          "",
          ""
        ],
        "3": [
          "Input1Offset",
          "0",
          "16",
          "29",
          false,
          "0019",
          "",
          ""
        ],
        "4": [
          "Input2Range",
          "1",
          "8",
          "31",
          false,
          "0020",
          "You must use wire bridges for current measurement!",
          ""
        ],
        "5": [
          "Input2Multiplier",
          "1",
          "16",
          "32",
          false,
          "0021",
          "",
          ""
        ],
        "6": [
          "Input2Divisor",
          "1",
          "16",
          "34",
          false,
          "0022",
          "",
          ""
        ],
        "7": [
          "Input2Offset",
          "0",
          "16",
          "36",
          false,
          "0023",
          "",
          ""
        ],
        "8": [
          "Input3Range",
          "1",
          "8",
          "38",
          false,
          "0024",
          "You must use wire bridges for current measurement!",
          ""
        ],
        "9": [
          "Input3Multiplier",
          "1",
          "16",
          "39",
          false,
          "0025",
          "",
          ""
        ],
        "10": [
          "Input3Divisor",
          "1",
          "16",
          "41",
          false,
          "0026",
          "",
          ""
        ],
        "11": [
          "Input3Offset",
          "0",
          "16",
          "43",
          false,
          "0027",
          "",
          ""
        ],
        "12": [
          "Input4Range",
          "1",
          "8",
          "45",
          false,
          "0028",
          "You must use wire bridges for current measurement!",
          ""
        ],
        "13": [
          "Input4Multiplier",
          "1",
          "16",
          "46",
          false,
          "0029",
          "",
          ""
        ],
        "14": [
          "Input4Divisor",
          "1",
          "16",
          "48",
          false,
          "0030",
          "",
          ""
        ],
        "15": [
          "Input4Offset",
          "0",
          "16",
          "50",
          false,
          "0031",
          "",
          ""
        ],
        "16": [
          "ADC_DataRate",
          "0",
          "8",
          "52",
          false,
          "0032",
          "Use lowest value for highest precision and a maximum 50 Hz suppression",
          ""
        ],
        "17": [
          "RTD1Type",
          "0",
          "8",
          "53",
          false,
          "0033",
          "",
          ""
        ],
        "18": [
          "RTD1Wiring",
          "0",
          "8",
          "54",
          false,
          "0034",
          "You must use wire bridges for 2-wire sensors!",
          ""
        ],
        "19": [
          "RTD1Multiplier",
          "1",
          "16",
          "55",
          false,
          "0035",
          "",
          ""
        ],
        "20": [
          "RTD1Divisor",
          "1",
          "16",
          "57",
          false,
          "0036",
          "",
          ""
        ],
        "21": [
          "RTD1Offset",
          "0",
          "16",
          "59",
          false,
          "0037",
          "",
          ""
        ],
        "22": [
          "RTD2Type",
          "0",
          "8",
          "61",
          false,
          "0038",
          "",
          ""
        ],
        "23": [
          "RTD2Wiring",
          "0",
          "8",
          "62",
          false,
          "0039",
          "You must use wire bridges for 2-wire sensors!",
          ""
        ],
        "24": [
          "RTD2Multiplier",
          "1",
          "16",
          "63",
          false,
          "0040",
          "",
          ""
        ],
        "25": [
          "RTD2Divisor",
          "1",
          "16",
          "65",
          false,
          "0041",
          "",
          ""
        ],
        "26": [
          "RTD2Offset",
          "0",
          "16",
          "67",
          false,
          "0042",
          "",
          ""
        ],
        "27": [
          "Output1Range",
          "2",
          "8",
          "69",
          false,
          "0043",
          "",
          ""
        ],
        "28": [
          "Output1EnableSlew",
          "0",
          "8",
          "70",
          false,
          "0044",
          "Enable slew rate deceleration",
          ""
        ],
        "29": [
          "Output1SlewStepSize",
          "0",
          "8",
          "71",
          false,
          "0045",
          "Slew rate step size",
          ""
        ],
        "30": [
          "Output1SlewClock",
          "0",
          "8",
          "72",
          false,
          "0046",
          "lock rate of slew rate deceleration in kHz",
          ""
        ],
        "31": [
          "Output1Multiplier",
          "1",
          "16",
          "73",
          false,
          "0047",
          "",
          ""
        ],
        "32": [
          "Output1Divisor",
          "1",
          "16",
          "75",
          false,
          "0048",
          "",
          ""
        ],
        "33": [
          "Output1Offset",
          "0",
          "16",
          "77",
          false,
          "0049",
          "",
          ""
        ],
        "34": [
          "Output2Range",
          "2",
          "8",
          "79",
          false,
          "0050",
          "",
          ""
        ],
        "35": [
          "Output2EnableSlew",
          "0",
          "8",
          "80",
          false,
          "0051",
          "Enable slew rate deceleration",
          ""
        ],
        "36": [
          "Output2SlewStepSize",
          "0",
          "8",
          "81",
          false,
          "0052",
          "Slew rate step size",
          ""
        ],
        "37": [
          "Output2SlewClock",
          "0",
          "8",
          "82",
          false,
          "0053",
          "lock rate of slew rate deceleration in kHz",
          ""
        ],
        "38": [
          "Output2Multiplier",
          "1",
          "16",
          "83",
          false,
          "0054",
          "",
          ""
        ],
        "39": [
          "Output2Divisor",
          "1",
          "16",
          "85",
          false,
          "0055",
          "",
          ""
        ],
        "40": [
          "Output2Offset",
          "0",
          "16",
          "87",
          false,
          "0056",
          "",
          ""
        ]
      },
      "extend": {}
    },
    {
      "GUID": "29a060b6-f3c9-7bc3-77aa-6cab36ee020a",
      "id": "device_RevPiMIO_20200901_1_0_001",
      "type": "LEFT_RIGHT",
      "productType": "118",
      "position": "33",
      "name": "RevPi MIO",
      "bmk": "RevPi MIO",
      "inpVariant": 0,
      "outVariant": 0,
      "comment": "This is Multi digital/analog inputs and outputs Device for RevPi",
      "offset": 346,
      "inp": {
        "0": [
          "MIO_R2_DI1",
          "0",
          "1",
          "0",
          true,
          "0000",
          "",
          "0"
        ],
        "1": [
          "MIO_R2_DI2",
          "0",
          "1",
          "0",
          true,
          "0001",
          "",
          "1"
        ],
        "2": [
          "MIO_R2_DI3",
          "0",
          "1",
          "0",
          true,
          "0002",
          "",
          "2"
        ],
        "3": [
          "MIO_R2_DI4",
          "0",
          "1",
          "0",
          true,
          "0003",
          "",
          "3"
        ],
        "4": [
          "ReservedDI_5",
          "null",
          "1",
          "0",
          true,
          "0004",
          "",
          "4"
        ],
        "5": [
          "ReservedDI_6",
          "null",
          "1",
          "0",
          true,
          "0005",
          "",
          "5"
        ],
        "6": [
          "ReservedDI_7",
          "null",
          "1",
          "0",
          true,
          "0006",
          "",
          "6"
        ],
        "7": [
          "ReservedDI_8",
          "null",
          "1",
          "0",
          true,
          "0007",
          "",
          "7"
        ],
        "8": [
          "DutyCycle_PulseLength_1",
          "0",
          "16",
          "1",
          true,
          "0008",
          "Dutycycle or PulseLength",
          ""
        ],
        "9": [
          "DutyCycle_PulseLength_2",
          "0",
          "16",
          "3",
          true,
          "0009",
          "Dutycycle or PulseLength",
          ""
        ],
        "10": [
          "DutyCycle_PulseLength_3",
          "0",
          "16",
          "5",
          true,
          "0010",
          "Dutycycle or PulseLength",
          ""
        ],
        "11": [
          "DutyCycle_PulseLength_4",
          "0",
          "16",
          "7",
          true,
          "0011",
          "Dutycycle or PulseLength",
          ""
        ],
        "12": [
          "Fpwm_PulseCount_1",
          "0",
          "16",
          "9",
          true,
          "0012",
          "Frequency or PulseCount DO",
          ""
        ],
        "13": [
          "Fpwm_PulseCount_2",
          "0",
          "16",
          "11",
          true,
          "0013",
          "Frequency or PulseCount DO",
          ""
        ],
        "14": [
          "Fpwm_PulseCount_3",
          "0",
          "16",
          "13",
          true,
          "0014",
          "Frequency or PulseCount DO",
          ""
        ],
        "15": [
          "Fpwm_PulseCount_4",
          "0",
          "16",
          "15",
          true,
          "0015",
          "Frequency or PulseCount DO",
          ""
        ],
        "16": [
          "AnalogInputLogicLevel_1",
          "0",
          "1",
          "17",
          false,
          "0016",
          "Logic Level (high/low) for analogInput",
          "0"
        ],
        "17": [
          "AnalogInputLogicLevel_2",
          "0",
          "1",
          "17",
          false,
          "0017",
          "Logic Level (high/low) for analogInput",
          "1"
        ],
        "18": [
          "AnalogInputLogicLevel_3",
          "0",
          "1",
          "17",
          false,
          "0018",
          "Logic Level (high/low) for analogInput",
          "2"
        ],
        "19": [
          "AnalogInputLogicLevel_4",
          "0",
          "1",
          "17",
          false,
          "0019",
          "Logic Level (high/low) for analogInput",
          "3"
        ],
        "20": [
          "AnalogInputLogicLevel_5",
          "0",
          "1",
          "17",
          false,
          "0020",
          "Logic Level (high/low) for analogInput",
          "4"
        ],
        "21": [
          "AnalogInputLogicLevel_6",
          "0",
          "1",
          "17",
          false,
          "0021",
          "Logic Level (high/low) for analogInput",
          "5"
        ],
        "22": [
          "AnalogInputLogicLevel_7",
          "0",
          "1",
          "17",
          false,
          "0022",
          "Logic Level (high/low) for analogInput",
          "6"
        ],
        "23": [
          "AnalogInputLogicLevel_8",
          "0",
          "1",
          "17",
          false,
          "0023",
          "Logic Level (high/low) for analogInput",
          "7"
        ],
        "24": [
          "MIO_R2_AI1",
          "0",
          "16",
          "18",
          true,
          "0024",
          "",
          ""
        ],
        "25": [
          "MIO_R2_AI2",
          "0",
          "16",
          "20",
          true,
          "0025",
          "",
          ""
        ],
        "26": [
          "MIO_R2_AI3",
          "0",
          "16",
          "22",
          true,
          "0026",
          "",
          ""
        ],
        "27": [
          "MIO_R2_AI4",
          "0",
          "16",
          "24",
          true,
          "0027",
          "",
          ""
        ],
        "28": [
          "MIO_R2_AI5",
          "0",
          "16",
          "26",
          true,
          "0028",
          "",
          ""
        ],
        "29": [
          "MIO_R2_AI6",
          "0",
          "16",
          "28",
          true,
          "0029",
          "",
          ""
        ],
        "30": [
          "MIO_R2_AI7",
          "0",
          "16",
          "30",
          true,
          "0030",
          "",
          ""
        ],
        "31": [
          "MIO_R2_AI8",
          "0",
          "16",
          "32",
          true,
          "0031",
          "",
          ""
        ]
      },
      "out": {
        "0": [
          "MIO_R2_DO1",
          "0",
          "1",
          "34",
          true,
          "0032",
          "",
          "0"
        ],
        "1": [
          "MIO_R2_DO2",
          "0",
          "1",
          "34",
          true,
          "0033",
          "",
          "1"
        ],
        "2": [
          "MIO_R2_DO3",
          "0",
          "1",
          "34",
          true,
          "0034",
          "",
          "2"
        ],
        "3": [
          "MIO_R2_DO4",
          "0",
          "1",
          "34",
          true,
          "0035",
          "",
          "3"
        ],
        "4": [
          "ReservedDO_5",
          "null",
          "1",
          "34",
          true,
          "0036",
          "",
          "4"
        ],
        "5": [
          "ReservedDO_6",
          "null",
          "1",
          "34",
          true,
          "0037",
          "",
          "5"
        ],
        "6": [
          "ReservedDO_7",
          "null",
          "1",
          "34",
          true,
          "0038",
          "",
          "6"
        ],
        "7": [
          "ReservedDO_8",
          "null",
          "1",
          "34",
          true,
          "0039",
          "",
          "7"
        ],
        "8": [
          "PwmDutycycle_1",
          "0",
          "16",
          "35",
          true,
          "0040",
          "",
          ""
        ],
        "9": [
          "PwmDutycycle_2",
          "0",
          "16",
          "37",
          true,
          "0041",
          "",
          ""
        ],
        "10": [
          "PwmDutycycle_3",
          "0",
          "16",
          "39",
          true,
          "0042",
          "",
          ""
        ],
        "11": [
          "PwmDutycycle_4",
          "0",
          "16",
          "41",
          true,
          "0043",
          "",
          ""
        ],
        "12": [
          "AnalogOutputLogicLevel_1",
          "0",
          "1",
          "43",
          false,
          "0044",
          "",
          "0"
        ],
        "13": [
          "AnalogOutputLogicLevel_2",
          "0",
          "1",
          "43",
          false,
          "0045",
          "",
          "1"
        ],
        "14": [
          "AnalogOutputLogicLevel_3",
          "0",
          "1",
          "43",
          false,
          "0046",
          "",
          "2"
        ],
        "15": [
          "AnalogOutputLogicLevel_4",
          "0",
          "1",
          "43",
          false,
          "0047",
          "",
          "3"
        ],
        "16": [
          "AnalogOutputLogicLevel_5",
          "0",
          "1",
          "43",
          false,
          "0048",
          "",
          "4"
        ],
        "17": [
          "AnalogOutputLogicLevel_6",
          "0",
          "1",
          "43",
          false,
          "0049",
          "",
          "5"
        ],
        "18": [
          "AnalogOutputLogicLevel_7",
          "0",
          "1",
          "43",
          false,
          "0050",
          "",
          "6"
        ],
        "19": [
          "AnalogOutputLogicLevel_8",
          "0",
          "1",
          "43",
          false,
          "0051",
          "",
          "7"
        ],
        "20": [
          "MIO_R2_AO1",
          "0",
          "16",
          "45",
          true,
          "0052",
          "",
          ""
        ],
        "21": [
          "MIO_R2_AO2",
          "0",
          "16",
          "47",
          true,
          "0053",
          "",
          ""
        ],
        "22": [
          "MIO_R2_AO3",
          "0",
          "16",
          "49",
          true,
          "0054",
          "",
          ""
        ],
        "23": [
          "MIO_R2_AO4",
          "0",
          "16",
          "51",
          true,
          "0055",
          "",
          ""
        ],
        "24": [
          "MIO_R2_AO5",
          "0",
          "16",
          "53",
          true,
          "0056",
          "",
          ""
        ],
        "25": [
          "MIO_R2_AO6",
          "0",
          "16",
          "55",
          true,
          "0057",
          "",
          ""
        ],
        "26": [
          "MIO_R2_AO7",
          "0",
          "16",
          "57",
          true,
          "0058",
          "",
          ""
        ],
        "27": [
          "MIO_R2_AO8",
          "0",
          "16",
          "59",
          true,
          "0059",
          "",
          ""
        ],
        "28": [
          "Reserved",
          "null",
          "8",
          "44",
          false,
          "0107",
          "",
          ""
        ]
      },
      "mem": {
        "0": [
          "EncoderMode",
          "0",
          "8",
          "61",
          false,
          "0060",
          "EncoderMode",
          ""
        ],
        "1": [
          "IO_Mode_1",
          "0",
          "8",
          "62",
          false,
          "0061",
          "Config digital IO mode",
          ""
        ],
        "2": [
          "IO_Mode_2",
          "3",
          "8",
          "63",
          false,
          "0062",
          "Config digital IO mode",
          ""
        ],
        "3": [
          "IO_Mode_3",
          "0",
          "8",
          "64",
          false,
          "0063",
          "Config digital IO mode",
          ""
        ],
        "4": [
          "IO_Mode_4",
          "3",
          "8",
          "65",
          false,
          "0064",
          "Config digital IO mode",
          ""
        ],
        "5": [
          "Pullup",
          "0",
          "8",
          "66",
          false,
          "0065",
          "",
          ""
        ],
        "6": [
          "PulseMode",
          "0",
          "8",
          "67",
          false,
          "0066",
          "",
          ""
        ],
        "7": [
          "FpwmOut_12",
          "0",
          "16",
          "68",
          false,
          "0067",
          "Pwm-Frequency of DO1 & DO2 in Hz",
          ""
        ],
        "8": [
          "FpwmOut_3",
          "0",
          "16",
          "70",
          false,
          "0068",
          "Pwm-Frequency of DO3 in Hz",
          ""
        ],
        "9": [
          "FpwmOut_4",
          "0",
          "16",
          "72",
          false,
          "0069",
          "Pwm-Frequency of DO4 in Hz",
          ""
        ],
        "10": [
          "PulseLength_1",
          "0",
          "16",
          "74",
          false,
          "0070",
          "Pulse-Length of DO [0-65535]",
          ""
        ],
        "11": [
          "PulseLength_2",
          "0",
          "16",
          "76",
          false,
          "0071",
          "Pulse-Length of DO [0-65535]",
          ""
        ],
        "12": [
          "PulseLength_3",
          "0",
          "16",
          "78",
          false,
          "0072",
          "Pulse-Length of DO [0-65535]",
          ""
        ],
        "13": [
          "PulseLength_4",
          "0",
          "16",
          "80",
          false,
          "0073",
          "Pulse-Length of DO [0-65535]",
          ""
        ],
        "14": [
          "AnalogInputMode_1",
          "0",
          "1",
          "82",
          false,
          "0074",
          "AnalogInput Mode",
          "0"
        ],
        "15": [
          "AnalogInputMode_2",
          "0",
          "1",
          "82",
          false,
          "0075",
          "AnalogInput Mode",
          "1"
        ],
        "16": [
          "AnalogInputMode_3",
          "0",
          "1",
          "82",
          false,
          "0076",
          "AnalogInput Mode",
          "2"
        ],
        "17": [
          "AnalogInputMode_4",
          "0",
          "1",
          "82",
          false,
          "0077",
          "AnalogInput Mode",
          "3"
        ],
        "18": [
          "AnalogInputMode_5",
          "0",
          "1",
          "82",
          false,
          "0078",
          "AnalogInput Mode",
          "4"
        ],
        "19": [
          "AnalogInputMode_6",
          "0",
          "1",
          "82",
          false,
          "0079",
          "AnalogInput Mode",
          "5"
        ],
        "20": [
          "AnalogInputMode_7",
          "0",
          "1",
          "82",
          false,
          "0080",
          "AnalogInput Mode",
          "6"
        ],
        "21": [
          "AnalogInputMode_8",
          "0",
          "1",
          "82",
          false,
          "0081",
          "AnalogInput Mode",
          "7"
        ],
        "22": [
          "InputLogicLevelVoltage_1",
          "0",
          "16",
          "83",
          false,
          "0082",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "23": [
          "InputLogicLevelVoltage_2",
          "0",
          "16",
          "85",
          false,
          "0083",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "24": [
          "InputLogicLevelVoltage_3",
          "0",
          "16",
          "87",
          false,
          "0084",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "25": [
          "InputLogicLevelVoltage_4",
          "0",
          "16",
          "89",
          false,
          "0085",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "26": [
          "InputLogicLevelVoltage_5",
          "0",
          "16",
          "91",
          false,
          "0086",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "27": [
          "InputLogicLevelVoltage_6",
          "0",
          "16",
          "93",
          false,
          "0087",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "28": [
          "InputLogicLevelVoltage_7",
          "0",
          "16",
          "95",
          false,
          "0088",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "29": [
          "InputLogicLevelVoltage_8",
          "0",
          "16",
          "97",
          false,
          "0089",
          "Switching threshold for analog inputs[0-10000]",
          ""
        ],
        "30": [
          "FilterWindowSize",
          "1",
          "8",
          "99",
          false,
          "0090",
          "Filter width of the moving average filter",
          ""
        ],
        "31": [
          "AnalogOutputMode_1",
          "0",
          "1",
          "100",
          false,
          "0091",
          "AnalogOutput Mode",
          "0"
        ],
        "32": [
          "AnalogOutputMode_2",
          "0",
          "1",
          "100",
          false,
          "0092",
          "AnalogOutput Mode",
          "1"
        ],
        "33": [
          "AnalogOutputMode_3",
          "0",
          "1",
          "100",
          false,
          "0093",
          "AnalogOutput Mode",
          "2"
        ],
        "34": [
          "AnalogOutputMode_4",
          "0",
          "1",
          "100",
          false,
          "0094",
          "AnalogOutput Mode",
          "3"
        ],
        "35": [
          "AnalogOutputMode_5",
          "0",
          "1",
          "100",
          false,
          "0095",
          "AnalogOutput Mode",
          "4"
        ],
        "36": [
          "AnalogOutputMode_6",
          "0",
          "1",
          "100",
          false,
          "0096",
          "AnalogOutput Mode",
          "5"
        ],
        "37": [
          "AnalogOutputMode_7",
          "0",
          "1",
          "100",
          false,
          "0097",
          "AnalogOutput Mode",
          "6"
        ],
        "38": [
          "AnalogOutputMode_8",
          "0",
          "1",
          "100",
          false,
          "0098",
          "AnalogOutput Mode",
          "7"
        ],
        "39": [
          "OutputLogicLevelVoltage_1",
          "0",
          "16",
          "101",
          false,
          "0099",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "40": [
          "OutputLogicLevelVoltage_2",
          "0",
          "16",
          "103",
          false,
          "0100",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "41": [
          "OutputLogicLevelVoltage_3",
          "0",
          "16",
          "105",
          false,
          "0101",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "42": [
          "OutputLogicLevelVoltage_4",
          "0",
          "16",
          "107",
          false,
          "0102",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "43": [
          "OutputLogicLevelVoltage_5",
          "0",
          "16",
          "109",
          false,
          "0103",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "44": [
          "OutputLogicLevelVoltage_6",
          "0",
          "16",
          "111",
          false,
          "0104",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "45": [
          "OutputLogicLevelVoltage_7",
          "0",
          "16",
          "113",
          false,
          "0105",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ],
        "46": [
          "OutputLogicLevelVoltage_8",
          "0",
          "16",
          "115",
          false,
          "0106",
          "Fixed output voltage for analog outputs[0-10000]",
          ""
        ]
      },
      "extend": {}
    }
  ],
  "Connections": []
}
