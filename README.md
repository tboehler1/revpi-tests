# revpi-tests

Test a Revolution Pi using `kyua` and `atf-sh`.

## Requirements

Install the dependencies:

- `kyua`
- `atf-sh`

## Usage

Clone this repository on the DUT and run `sudo make check`.

Alternatively, the test can run with an unprivileged user, but some tests may
be skipped due to the privileged user requirements.

## Configuration

The configuration file is `kyua.conf` and lists variables for a device that is
tested.

Every device relies on a Pictory configuration with a specific naming scheme
for tests that use the PiBridge (see [`pb_test`](./pb_test) and the [naming
schema for Pictory
configurations](https://gitlab.com/revolutionpi/infrastructure/testing/LAVA-test-definitions#naming-schema-for-pictory-configurations)).

The configuration variables should be self-explanatory.

## Setup

### RS485 Test

In order for RS485 testing to work the DUT should be a device that supports
RS485 (i.e. Connect, Flat) and be connected to a test station. On the test
station, the RS485 server found here should be running:
https://gitlab.com/revolutionpi/infrastructure/testing/LAVA-test-definitions/-/blob/master/automated/revpi/rs485/rs485.py?ref_type=heads

The variable `RSDEV` is the block device where the serial connection happens.

The baud rate should be `19200`.

### ETH-3 Test (iperf)

For using the iperf test (ETH-3), a remote test station must be running iperf.

## Notes

- The tests for the PiBridge *can* run without root privileges but only if the
  RSC file specified in the Kyua configuration is installed into
  `/etc/revpi/config.rsc`. If the files `/etc/revpi/config.rsc` and the DUTs
  configuration from the tests repo differ not all PiBridge tests are run.
- The USB tests cannot (and won't) be run in parallel with any other tests.
- This has only been tested with a 32 GB Connect 4 without WiFi, as well as a
  Connect 5. Other devices will require adjusting the configuration and tests.


## Example

The project includes an example Kyua and Pictory configuration for a Connect 4
as well as a Connect 5 with some modules attached.
