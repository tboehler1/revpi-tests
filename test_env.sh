init_env() {
	:
}

init_tests() {
	export TESTS="$@"

	for i; do
		atf_test_case "$i"
	done
}

atf_init_test_cases() {
	for i in $TESTS; do
		atf_add_test_case "$i"
	done
}

should_run_test() {
	local current="$1"
	local tests="$2"

	# make sure PT-1 isn't matched by PT-11, for example
	for test in $tests; do
		if echo "$test" | grep -qFx "$current"; then
			return 0
		fi
	done

	atf_skip "Setup doesn't support test '$current'"
}

check_available_dev() {
	local dev="${1:?No device specified}"

	if [ ! -b "$mount_dev" ]; then
		atf_skip "Block device $mount_dev not found"
	fi
}

ensure_mountable_partition() {
	local dev="${1:?No device specified}"
	local part="${2:?No device partition specified}"

	check_available_dev "$dev"

	if [ ! -b "$mount_part" ] || ! (file -Ls "$mount_part" | grep -q ext4); then
		partition_disk $mount_dev $mount_part || return 1
	fi
}

ensure_config_rsc() {
	if ! $(atf_config_has RSC_CONFIG); then
		printf "Cannot ensure config.rsc is updated.\n"
		return 1
	fi

	local rsc="$(atf_get_srcdir)/files/$(atf_config_get RSC_CONFIG)"
	if command -v sha256sum 2>&1 >/dev/null; then
		local sha_old="$(sha256sum "$CONFIG_RSC_PATH")"
		local sha_new="$(sha256sum "$rsc")"

		if [ "${sha_old%% *}" = "${sha_new%% *}" ]; then
			printf "config.rsc up-to-date, nothing to do\n"
			return 0
		fi
	else
		printf "Missing sha256sum to check for outdated config.rsc.\n"
	fi

	if [ "$(id -u)" -ne "0" ]; then
		printf "No permission to update config.rsc.\n"
		return 1
	fi

	# it's nonsense to update the config.rsc without reloading piControl
	atf_require_prog piTest

	cp -v "$rsc" "$CONFIG_RSC_PATH" \
		|| atf_fail \
		"config.rsc file '$rsc' cannot be copied to $CONFIG_RSC_PATH"

	atf_check \
		piTest -x
}
